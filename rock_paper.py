import sys

player1 = str(input("player1: rock, paper, scissor?: "))
player2 = str(input("player2: rock, paper, scissor?: "))

if player1==player2:
    print("same, play again")
    sys.exit()
elif player1=="rock":
    if player2=="paper":
        print("paper wins")
    else:
        print("rock wins")

elif player1=="paper":
    if player2=="rock":
        print("paper wins")
    else:
        print("scissor wins")

elif player1=="scissor":
    if player2=="paper":
        print("scissor wins")
    else:
        print("rock wins")
else:
    print("invalid input")
    sys.exit()